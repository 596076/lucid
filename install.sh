#!/bin/sh

#move to script directory so all relative paths work
cd "$(dirname "$0")"

#set executable permison on all scripts
find . -type f |grep -i '.sh$' | xargs chmod 750

#includes
. ./resources/config.sh

#update repo
apt-get update



#Add dependencies
apt-get install --no-install-recommends -y build-essential
apt-get install --no-install-recommends -y net-tools
apt-get install -y curl
apt-get install -y ca-certificates
apt-get install --no-install-recommends -y sqlite3
apt-get install --no-install-recommends -y python3-pip
apt-get install --no-install-recommends -y nginx
apt-get install --no-install-recommends -y expect


#set some systemd configs
cp resources/systemd/* /lib/systemd/system/
systemctl daemon-reload
systemctl enable supervisord
systemctl enable lucid && systemctl start lucid

#Python
resources/python.sh

#NGINX web server
resources/nginx.sh


#copy some supervisord config files
mkdir -p /etc/supervisord/conf.d
mkdir -p /var/log/supervisor
cp resources/supervisord/supervisor.conf /etc/supervisord/
cp resources/supervisord/validator.conf /etc/supervisord/conf.d/
systemctl restart supervisord

#create apps dir where we store all python apps
mkdir -p /apps/validator/
cp resources/validator_api/main.py /apps/validator/

#create db
resources/database.sh


#set executable permison on all scripts
find . -type f |grep -i '.sh$' | xargs chmod 750

