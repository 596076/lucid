---
```
                                        # API Schema #
```
---

![api schema](resources/images/schema.png)

&nbsp;
&nbsp;

> 1.  Users have to provide their credentials within the headers of the http package
> 1.  Nginx forward the package to the authentication api
> 1.  Authentication api validate the credentials and check the local sqlite db
>     - if Auth API returns status code 200 Nginx will make get request to backend API
>     - if Auth API returns status code != 200 then nginx will return not authorized message to the client
> 1.  Backend send response data to Nginx which arfterwards forwards to the client

```
  example:
  ----
  curl -X GET \
  -k https://ip \
  -H 'password: password' \
  -H 'username: username'
```

&nbsp;
&nbsp;

### TODO

- [ ] Log failed/succesful requests in authentication api
- [ ] Implement authentication with JWT
